<?php

namespace CookMyChoiceBundle\Entity;

/**
 * Image
 */
class Image
{
    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \CookMyChoiceBundle\Entity\Plat
     */
    private $plat;


    /**
     * Set image
     *
     * @param string $image
     *
     * @return Image
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Image
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set plat
     *
     * @param \CookMyChoiceBundle\Entity\Plat $plat
     *
     * @return Image
     */
    public function setPlat(\CookMyChoiceBundle\Entity\Plat $plat = null)
    {
        $this->plat = $plat;

        return $this;
    }

    /**
     * Get plat
     *
     * @return \CookMyChoiceBundle\Entity\Plat
     */
    public function getPlat()
    {
        return $this->plat;
    }
}

