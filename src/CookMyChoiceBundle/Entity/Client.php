<?php

namespace CookMyChoiceBundle\Entity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Client
 */
class Client implements UserInterface
{
    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prenom;

    /**
     * @var string
     */
    private $eMail;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $commandes;

    /**
     * @var \CookMyChoiceBundle\Entity\Statut
     */
    private $statut;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->commandes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Client
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Client
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set eMail
     *
     * @param string $eMail
     *
     * @return Client
     */
    public function setEMail($eMail)
    {
        $this->eMail = $eMail;

        return $this;
    }

    /**
     * Get eMail
     *
     * @return string
     */
    public function getEMail()
    {
        return $this->eMail;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return Client
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add commande
     *
     * @param \CookMyChoiceBundle\Entity\Commande $commande
     *
     * @return Client
     */
    public function addCommande(\CookMyChoiceBundle\Entity\Commande $commande)
    {
        $this->commandes[] = $commande;

        return $this;
    }

    /**
     * Remove commande
     *
     * @param \CookMyChoiceBundle\Entity\Commande $commande
     */
    public function removeCommande(\CookMyChoiceBundle\Entity\Commande $commande)
    {
        $this->commandes->removeElement($commande);
    }

    /**
     * Get commandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommandes()
    {
        return $this->commandes;
    }

    /**
     * Set statut
     *
     * @param \CookMyChoiceBundle\Entity\Statut $statut
     *
     * @return Client
     */
    public function setStatut(\CookMyChoiceBundle\Entity\Statut $statut = null)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return \CookMyChoiceBundle\Entity\Statut
     */
    public function getStatut()
    {
        return $this->statut;
    }
    
    //
    
    private $passwordOriginal;
    
    public function getPasswordOriginal() {
        return $this->passwordOriginal;
    }

    public function setPasswordOriginal($passwordOriginal) {
        $this->passwordOriginal = $passwordOriginal;
    }
    public function eraseCredentials()  
    {
        
    }
    
     public function getRoles()
    {
        return array('ROLE_USER') ;
    }
    
     public function getSalt()
    {
        return null;
    }
    
     public function getUsername()  
    {
         return $this->login;
    }
}

