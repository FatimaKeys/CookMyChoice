<?php

namespace CookMyChoiceBundle\Entity;

/**
 * Commande
 */
class Commande
{
    /**
     * @var string
     */
    private $numCommande;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $quantite;

    /**
     * @var integer
     */
    private $montantCommande;

    /**
     * @var string
     */
    private $modePaiement;

    /**
     * @var integer
     */
    private $numTable;

    /**
     * @var integer
     */
    private $nbrPersonne;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $delaisPlats;

    /**
     * @var \CookMyChoiceBundle\Entity\Client
     */
    private $client;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->delaisPlats = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set numCommande
     *
     * @param string $numCommande
     *
     * @return Commande
     */
    public function setNumCommande($numCommande)
    {
        $this->numCommande = $numCommande;

        return $this;
    }

    /**
     * Get numCommande
     *
     * @return string
     */
    public function getNumCommande()
    {
        return $this->numCommande;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Commande
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return Commande
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set montantCommande
     *
     * @param integer $montantCommande
     *
     * @return Commande
     */
    public function setMontantCommande($montantCommande)
    {
        $this->montantCommande = $montantCommande;

        return $this;
    }

    /**
     * Get montantCommande
     *
     * @return integer
     */
    public function getMontantCommande()
    {
        return $this->montantCommande;
    }

    /**
     * Set modePaiement
     *
     * @param string $modePaiement
     *
     * @return Commande
     */
    public function setModePaiement($modePaiement)
    {
        $this->modePaiement = $modePaiement;

        return $this;
    }

    /**
     * Get modePaiement
     *
     * @return string
     */
    public function getModePaiement()
    {
        return $this->modePaiement;
    }

    /**
     * Set numTable
     *
     * @param integer $numTable
     *
     * @return Commande
     */
    public function setNumTable($numTable)
    {
        $this->numTable = $numTable;

        return $this;
    }

    /**
     * Get numTable
     *
     * @return integer
     */
    public function getNumTable()
    {
        return $this->numTable;
    }

    /**
     * Set nbrPersonne
     *
     * @param integer $nbrPersonne
     *
     * @return Commande
     */
    public function setNbrPersonne($nbrPersonne)
    {
        $this->nbrPersonne = $nbrPersonne;

        return $this;
    }

    /**
     * Get nbrPersonne
     *
     * @return integer
     */
    public function getNbrPersonne()
    {
        return $this->nbrPersonne;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add delaisPlat
     *
     * @param \CookMyChoiceBundle\Entity\DelaiPlat $delaisPlat
     *
     * @return Commande
     */
    public function addDelaisPlat(\CookMyChoiceBundle\Entity\DelaiPlat $delaisPlat)
    {
        $this->delaisPlats[] = $delaisPlat;

        return $this;
    }

    /**
     * Remove delaisPlat
     *
     * @param \CookMyChoiceBundle\Entity\DelaiPlat $delaisPlat
     */
    public function removeDelaisPlat(\CookMyChoiceBundle\Entity\DelaiPlat $delaisPlat)
    {
        $this->delaisPlats->removeElement($delaisPlat);
    }

    /**
     * Get delaisPlats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDelaisPlats()
    {
        return $this->delaisPlats;
    }

    /**
     * Set client
     *
     * @param \CookMyChoiceBundle\Entity\Client $client
     *
     * @return Commande
     */
    public function setClient(\CookMyChoiceBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CookMyChoiceBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}

