<?php

namespace CookMyChoiceBundle\Entity;

/**
 * Ingredient
 */
class Ingredient
{
    /**
     * @var string
     */
    private $nomIgredient;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $platIngredients;

    /**
     * @var \CookMyChoiceBundle\Entity\Categorie
     */
    private $categorie;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->platIngredients = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nomIgredient
     *
     * @param string $nomIgredient
     *
     * @return Ingredient
     */
    public function setNomIgredient($nomIgredient)
    {
        $this->nomIgredient = $nomIgredient;

        return $this;
    }

    /**
     * Get nomIgredient
     *
     * @return string
     */
    public function getNomIgredient()
    {
        return $this->nomIgredient;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add platIngredient
     *
     * @param \CookMyChoiceBundle\Entity\PlatIngredient $platIngredient
     *
     * @return Ingredient
     */
    public function addPlatIngredient(\CookMyChoiceBundle\Entity\PlatIngredient $platIngredient)
    {
        $this->platIngredients[] = $platIngredient;

        return $this;
    }

    /**
     * Remove platIngredient
     *
     * @param \CookMyChoiceBundle\Entity\PlatIngredient $platIngredient
     */
    public function removePlatIngredient(\CookMyChoiceBundle\Entity\PlatIngredient $platIngredient)
    {
        $this->platIngredients->removeElement($platIngredient);
    }

    /**
     * Get platIngredients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlatIngredients()
    {
        return $this->platIngredients;
    }

    /**
     * Set categorie
     *
     * @param \CookMyChoiceBundle\Entity\Categorie $categorie
     *
     * @return Ingredient
     */
    public function setCategorie(\CookMyChoiceBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \CookMyChoiceBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
}

