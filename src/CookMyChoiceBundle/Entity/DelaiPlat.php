<?php

namespace CookMyChoiceBundle\Entity;

/**
 * DelaiPlat
 */
class DelaiPlat
{
    /**
     * @var integer
     */
    private $nbrPlat;

    /**
     * @var integer
     */
    private $delai;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \CookMyChoiceBundle\Entity\Commande
     */
    private $commande;

    /**
     * @var \CookMyChoiceBundle\Entity\Plat
     */
    private $plat;


    /**
     * Set nbrPlat
     *
     * @param integer $nbrPlat
     *
     * @return DelaiPlat
     */
    public function setNbrPlat($nbrPlat)
    {
        $this->nbrPlat = $nbrPlat;

        return $this;
    }

    /**
     * Get nbrPlat
     *
     * @return integer
     */
    public function getNbrPlat()
    {
        return $this->nbrPlat;
    }

    /**
     * Set delai
     *
     * @param integer $delai
     *
     * @return DelaiPlat
     */
    public function setDelai($delai)
    {
        $this->delai = $delai;

        return $this;
    }

    /**
     * Get delai
     *
     * @return integer
     */
    public function getDelai()
    {
        return $this->delai;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set commande
     *
     * @param \CookMyChoiceBundle\Entity\Commande $commande
     *
     * @return DelaiPlat
     */
    public function setCommande(\CookMyChoiceBundle\Entity\Commande $commande = null)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return \CookMyChoiceBundle\Entity\Commande
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * Set plat
     *
     * @param \CookMyChoiceBundle\Entity\Plat $plat
     *
     * @return DelaiPlat
     */
    public function setPlat(\CookMyChoiceBundle\Entity\Plat $plat = null)
    {
        $this->plat = $plat;

        return $this;
    }

    /**
     * Get plat
     *
     * @return \CookMyChoiceBundle\Entity\Plat
     */
    public function getPlat()
    {
        return $this->plat;
    }
}

