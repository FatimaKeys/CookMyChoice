<?php

namespace CookMyChoiceBundle\Entity;

/**
 * PlatIngredient
 */
class PlatIngredient
{
    /**
     * @var string
     */
    private $remarque;

    /**
     * @var integer
     */
    private $nbrIngredient;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \CookMyChoiceBundle\Entity\Plat
     */
    private $plat;

    /**
     * @var \CookMyChoiceBundle\Entity\Ingredient
     */
    private $ingredient;


    /**
     * Set remarque
     *
     * @param string $remarque
     *
     * @return PlatIngredient
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set nbrIngredient
     *
     * @param integer $nbrIngredient
     *
     * @return PlatIngredient
     */
    public function setNbrIngredient($nbrIngredient)
    {
        $this->nbrIngredient = $nbrIngredient;

        return $this;
    }

    /**
     * Get nbrIngredient
     *
     * @return integer
     */
    public function getNbrIngredient()
    {
        return $this->nbrIngredient;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set plat
     *
     * @param \CookMyChoiceBundle\Entity\Plat $plat
     *
     * @return PlatIngredient
     */
    public function setPlat(\CookMyChoiceBundle\Entity\Plat $plat = null)
    {
        $this->plat = $plat;

        return $this;
    }

    /**
     * Get plat
     *
     * @return \CookMyChoiceBundle\Entity\Plat
     */
    public function getPlat()
    {
        return $this->plat;
    }

    /**
     * Set ingredient
     *
     * @param \CookMyChoiceBundle\Entity\Ingredient $ingredient
     *
     * @return PlatIngredient
     */
    public function setIngredient(\CookMyChoiceBundle\Entity\Ingredient $ingredient = null)
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    /**
     * Get ingredient
     *
     * @return \CookMyChoiceBundle\Entity\Ingredient
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }
}

