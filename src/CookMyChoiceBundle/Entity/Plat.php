<?php

namespace CookMyChoiceBundle\Entity;

/**
 * Plat
 */
class Plat
{
    /**
     * @var string
     */
    private $nomPlat;

    /**
     * @var integer
     */
    private $prix;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $delaisPlats;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $platIngredients;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->delaisPlats = new \Doctrine\Common\Collections\ArrayCollection();
        $this->platIngredients = new \Doctrine\Common\Collections\ArrayCollection();
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nomPlat
     *
     * @param string $nomPlat
     *
     * @return Plat
     */
    public function setNomPlat($nomPlat)
    {
        $this->nomPlat = $nomPlat;

        return $this;
    }

    /**
     * Get nomPlat
     *
     * @return string
     */
    public function getNomPlat()
    {
        return $this->nomPlat;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     *
     * @return Plat
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return integer
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add delaisPlat
     *
     * @param \CookMyChoiceBundle\Entity\DelaiPlat $delaisPlat
     *
     * @return Plat
     */
    public function addDelaisPlat(\CookMyChoiceBundle\Entity\DelaiPlat $delaisPlat)
    {
        $this->delaisPlats[] = $delaisPlat;

        return $this;
    }

    /**
     * Remove delaisPlat
     *
     * @param \CookMyChoiceBundle\Entity\DelaiPlat $delaisPlat
     */
    public function removeDelaisPlat(\CookMyChoiceBundle\Entity\DelaiPlat $delaisPlat)
    {
        $this->delaisPlats->removeElement($delaisPlat);
    }

    /**
     * Get delaisPlats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDelaisPlats()
    {
        return $this->delaisPlats;
    }

    /**
     * Add platIngredient
     *
     * @param \CookMyChoiceBundle\Entity\PlatIngredient $platIngredient
     *
     * @return Plat
     */
    public function addPlatIngredient(\CookMyChoiceBundle\Entity\PlatIngredient $platIngredient)
    {
        $this->platIngredients[] = $platIngredient;

        return $this;
    }

    /**
     * Remove platIngredient
     *
     * @param \CookMyChoiceBundle\Entity\PlatIngredient $platIngredient
     */
    public function removePlatIngredient(\CookMyChoiceBundle\Entity\PlatIngredient $platIngredient)
    {
        $this->platIngredients->removeElement($platIngredient);
    }

    /**
     * Get platIngredients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlatIngredients()
    {
        return $this->platIngredients;
    }

    /**
     * Add image
     *
     * @param \CookMyChoiceBundle\Entity\Image $image
     *
     * @return Plat
     */
    public function addImage(\CookMyChoiceBundle\Entity\Image $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \CookMyChoiceBundle\Entity\Image $image
     */
    public function removeImage(\CookMyChoiceBundle\Entity\Image $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
}

