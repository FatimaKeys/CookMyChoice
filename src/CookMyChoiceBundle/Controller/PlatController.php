<?php

namespace CookMyChoiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use CookMyChoiceBundle\Form\ClientType;
use CookMyChoiceBundle\Entity\Client;
use CookMyChoiceBundle\Entity\Ingredient;
use CookMyChoiceBundle\Entity\Plat;
use CookMyChoiceBundle\Entity\PlatIngredient;
use CookMyChoiceBundle\Entity\Categorie;
use Symfony\Component\HttpFoundation\JsonResponse;

class PlatController extends Controller
{

//    public function afficherCategoriesAction()
//    {
//        $em=$this->getDoctrine()->getManager();
//        $repositoryCategorie = $em -> getRepository(Categorie::Class);
//        $categories= $repositoryCategorie->findAll();
//        $vars=['lesCategories'=>$categories];
//        return $this->render ('CookMyChoiceBundleView/afficher_categories.html.twig',$vars);          
//    }


    public function trouverIngredientAjaxAction(Request $req) 
    { 
        $em=$this->getDoctrine()->getManager();
        $categorie=$em->getRepository(Categorie::Class)->findOneBy(array ('nomCategorie'=>$req->get('userChoice')));
        $ingredients= $em->getRepository(Ingredient::Class)->findBy(array ('categorie'=>$categorie));
        $nomIngredients=[];
        
        //recuperer seulement le string de l objet 
        foreach ($ingredients as $ing){
         $nomIngredients[]=$ing->getNomIgredient();
        }
        $arrayReponse = ['nomIngredients'=>$nomIngredients
                          ]; 
        return new JsonResponse ($arrayReponse);
    }
     public function trouverPlatAction(Request $req) 
    { 
//         dump($req->get("ingredient1"));
//         dump($req->get("ingredient2"));
//         dump($req->get("ingredient3"));
//         die();
         
         $em=$this->getDoctrine()->getManager();
         $categorie1=$em->getRepository(Categorie::Class)->findOneBy(array ('nomCategorie'=>$req->get('categorie1')));
         $ingredient1=$em->getRepository(Ingredient::Class)->findBy(array ('nomIgredient'=>$req->get("ingredient1"),'categorie'=>$categorie1));
         //dump($ingredient1[0]->getPlatIngredients()[0]->getPlat());
          $vars=['lesPlatsIng'=>$ingredient1[0]->getPlatIngredients()];
         return  $this->render ('CookMyChoiceBundleView/afficher_plat.html.twig',$vars); 
    }
    
    public function afficherPlatsContenantIngredientAction()
    {
        //rechercher tous les plats qui contiennent un des ingredient (DQL SELECT....WHERE idIngredient... OR ....OR°
        
    }
    public function afficherImagesPlatsAction()
    {
      
    }    //rechercher tous les plats qui contiennent un des ingredient (DQL SELECT....WHERE idIngredient... OR ....OR°
}
