<?php

namespace CookMyChoiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    public function choixlangueAction()
    {
        return $this->render('CookMyChoiceBundleView/choixlangue.html.twig');
    }
}


