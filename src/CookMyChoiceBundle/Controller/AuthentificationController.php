<?php

namespace CookMyChoiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use CookMyChoiceBundle\Form\ClientType;
use CookMyChoiceBundle\Entity\Client;
use CookMyChoiceBundle\Entity\Categorie;
use CookMyChoiceBundle\Entity\Ingredient;

class AuthentificationController extends Controller
{
    public function inscriptionAction(Request $req)//que doit faire cette action? afficher le formulaire 
    {
        $passwordEncoder= $this->get ('security.password_encoder');
        $client = new Client();
        $formulaire = $this->createForm (ClientType::class, $client);
        $formulaire->handleRequest($req);
        if ($formulaire->isValid() &&  $formulaire->isSubmitted()){
            $password = $passwordEncoder->encodePassword($client, $client->getPasswordOriginal());
            $client->setPassword($password);
            //inscerer dans la DB
            $em = $this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();
            
            //rediriger vers le login
            return $this->redirectToRoute('connexion');
        }
        else
        {
            //envoyer l action au twig
        $vars = ['formulaire'=>$formulaire->createView()]; 
        return $this->render('CookMyChoiceBundleView\Authentification\inscription.html.twig',$vars);
        }
         
    }
    
    public function connexionAction()
   {
        $objetAuthentification = $this->get ('security.authentication_utils');
        $erreurs = $objetAuthentification->getLastAuthenticationError();
        $login = $objetAuthentification->getLastUsername();
        
        $vars = ['erreurs' => $erreurs,
                'login' => $login];

      return $this->render ('CookMyChoiceBundleView\Authentification\connexion.html.twig',$vars);
    }

    public function afficherClientAction()
    {
        $em=$this->getDoctrine()->getManager();
        $repositoryCategorie = $em -> getRepository(Categorie::Class);
        $categories= $repositoryCategorie->findAll();
        
        $ingredients=$em -> getRepository(Ingredient::Class)->findAll();
        $vars=['lesCategories'=>$categories,'lesIngredients'=>$ingredients];
        return 
        $this->render('CookMyChoiceBundleView\Authentification\afficherclient.html.twig', $vars);
    }
    public function seDeconneterAction()
    {
        
    }
   
}
