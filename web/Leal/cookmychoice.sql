-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 13 avr. 2018 à 10:49
-- Version du serveur :  10.1.24-MariaDB
-- Version de PHP :  7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `cookmychoice`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `nomCategorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nomCategorie`) VALUES
(1, 'Fruits'),
(2, 'Légumes'),
(4, 'Féculents'),
(5, 'Produits laitiers'),
(6, 'Viandes'),
(7, 'Poissons'),
(8, 'Matières grasses'),
(9, 'Oeufs'),
(10, 'Epices');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eMail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statut_id` int(11) DEFAULT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `prenom`, `eMail`, `statut_id`, `login`, `password`) VALUES
(4, 'a', 'a', 'a@zerzer.es', NULL, 'z', '$2y$13$a/NF4EuHLDxLVxPg6yPP7.zVzBssKRGsbmlLqBneZTAXQCb3Hq57a'),
(5, 'aa', 'a', 'a@q.a', NULL, 'a', '$2y$13$t1V4UJwzjx9leK8CFC13NuMICcHm5g9XAiKNw00f47gBWHAMum9Ki'),
(6, 'Fatima', 'Meft', 'fat@gmail.com', NULL, '1234', '$2y$13$5uE13Z54qEiYPEzQaRHgyeObJxHZF87y9R.ZPh1xxDSTyWWvdwhKe'),
(7, 'Nadia', 'Nad', 'nad@gmail.com', NULL, '456', '$2y$13$igqJHAdbm6L0RzcsEiYlMed6xNiRcH3d/gBsLconTpZG1qJsgZ5i.'),
(8, 'Noel', 'no', 'no@gmail.com', NULL, '123', '$2y$13$/N1gTnHBhU3KvS3CMdsYAOgTe8W8m2Y8HlOfhm7YVJYRkVU5va3r2'),
(9, 'Lucia', 'Loulou', 'lou@hotmail.com', NULL, '1234', '$2y$13$x93utSN0/3scuJP2aXSjnuIonJoi.eyKCo.31XI93trSSy2A3.RYO'),
(10, 'Loulou', 'Lucia', 'd@gmail.com', NULL, 'loulou', '$2y$13$tFZoWPXrSb84ZEej46c3W.NmeFusBYbU0.C2tGZODV7a6G/ssPnN6'),
(11, 'Trang', 'trang', 'trang@gmail.com', NULL, 'trang', '$2y$13$6EysZ/u6BI/x4lUh2gzY8uPSJx89nOioYaXxizrKov2cKRuU2r.Ri'),
(12, 'Celine', 'Celine', 'celine@gmail.com', NULL, 'celine', '$2y$13$iPfIIKrw8O/mI3H2E25diuq1HFdKz9HPRuhvodpBXq2XANYSTg1h.'),
(13, 'Vanessa', 'VAN', 'VAN@GMAIL.COM', NULL, 'van123', '$2y$13$/DkC76LTBHP5eSvmpEvi2O5eY9dPzmoOi1kMOinC.DgNQjGX2SdY.'),
(14, 'hello', 'hello', 'hello@gmail.com', NULL, '159', '$2y$13$HQnbqjw6b7NWtTzx8dALNePBMIfS7cNXu3w8oHCE28YM8ldudlp3y'),
(15, 'Keys', 'Fatima', 'fatima@gmail.com', NULL, 'Test1234', '$2y$13$WaTi/Z2vwOlSOsKZkwvaM.jDhneqfuI4MDrsYge0nNKK6THbUt6mO');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` int(11) NOT NULL,
  `numCommande` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` datetime NOT NULL,
  `quantite` int(11) NOT NULL,
  `montantCommande` int(11) NOT NULL,
  `modePaiement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `numTable` int(11) NOT NULL,
  `nbrPersonne` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `delai_plat`
--

CREATE TABLE `delai_plat` (
  `id` int(11) NOT NULL,
  `nbrPlat` int(11) DEFAULT NULL,
  `delai` int(11) NOT NULL,
  `commande_id` int(11) DEFAULT NULL,
  `plat_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `delai_plat`
--

INSERT INTO `delai_plat` (`id`, `nbrPlat`, `delai`, `commande_id`, `plat_id`) VALUES
(1, NULL, 40, NULL, 1),
(2, NULL, 25, NULL, 2),
(3, NULL, 15, NULL, 3),
(4, NULL, 20, NULL, 6),
(5, NULL, 40, NULL, 5),
(6, NULL, 20, NULL, 4);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plat_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`id`, `image`, `libelle`, `plat_id`) VALUES
(4, 'gratin-d-aubergine-tomate-chevre-basilic.jpg', 'Gratin d\'aubergines aux tomates et au chèvre', 1),
(5, 'millefeuille-aubergine-chèvre-poivron-olive-basilic.jpg', 'Mille feuille d\'aubergine au chèvre et légumes grillès', 2),
(6, 'tartines-de-legumes-tomate-poivron-chevre-grilles.jpg', 'Tartines de legumes grillès ', 3),
(7, 'salad-asperge-chicon.jpg', 'Salade printanière d\'endives et d\'asperges', 4),
(8, 'saumon-pommedeterre-salade-asperge.jpg', 'Pavé de Saumon écossais poêlée de pommes ratte et asperges vertes au safran', 5),
(9, 'saumon-tartartdefromage-asparagus-green.jpg', 'Bavarois d\'asperge au saumon et ricotta', 6);

-- --------------------------------------------------------

--
-- Structure de la table `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL,
  `nomIgredient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categorie_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ingredient`
--

INSERT INTO `ingredient` (`id`, `nomIgredient`, `categorie_id`) VALUES
(1, 'aubergine', 2),
(2, 'asperge', 2),
(3, 'brocoli', 2),
(4, 'champignon', 2),
(5, 'chou de Bruxelles\r\n', 2),
(6, 'chou-fleur', 2),
(7, 'concombre', 2),
(8, 'courgette', 2),
(9, 'chicon', 2),
(10, 'épinard', 2),
(11, 'haricot', 2),
(12, 'laitue', 2),
(13, 'maïs', 2),
(14, 'oignon\r\n', 2),
(15, 'poireau', 2),
(16, 'poivron', 2),
(17, 'pomme de terre', 2),
(18, 'pomme de terre', 4),
(19, 'potiron', 2),
(20, 'tomate', 2),
(21, 'tomate', 1),
(22, 'fenouil', 2),
(23, 'panais', 2),
(24, 'fraise', 1),
(25, 'melon', 1),
(26, 'myrtille', 1),
(27, 'poire', 1),
(28, 'pomme', 1),
(29, 'prune', 1),
(30, 'citon', 1),
(31, 'abricot', 1),
(32, 'ananas', 1),
(33, 'banane', 1),
(34, 'kiwi', 1),
(35, 'litchi', 1),
(36, 'orange', 1),
(37, 'raisin', 1),
(38, 'patate douce', 4),
(39, 'riz', 4),
(40, 'pâtes\r\n', 4),
(41, 'quinoa', 4),
(42, 'crème', 5),
(43, 'lait', 5),
(44, 'mascarpon', 5),
(45, 'ricota', 5),
(46, 'mozzarela', 5),
(47, 'feta', 5),
(48, 'fromage', 5),
(49, 'bœuf', 6),
(50, 'veau ', 6),
(51, 'mouton', 6),
(52, 'agneau', 6),
(53, 'chèvre', 6),
(54, 'volailles ', 6),
(55, 'gibier ', 6),
(56, 'lapin', 6),
(57, 'dorade', 7),
(58, 'lotte\r\n', 7),
(59, 'merlan', 7),
(60, 'rouget', 7),
(61, 'sole', 7),
(62, 'bar', 7),
(63, 'pangasius', 7),
(64, 'esturgeon', 7),
(65, 'sardine\r\n', 7),
(66, 'saumon ', 7),
(67, 'thon', 7),
(68, 'oeuf', 9),
(69, 'huile d\'olive', 8),
(70, 'huile de truffe\r\n', 8),
(71, 'huile de noix', 8),
(72, 'huile d\'amande', 8),
(73, 'huile aux agrumes', 8),
(74, 'couscous', 4),
(130, 'aneth', 10),
(131, 'persil', 10),
(132, 'estragon', 10),
(133, 'laurier ', 10),
(134, 'menthe ', 10),
(135, 'coriandre ', 10),
(136, 'curcuma ', 10),
(137, 'paprika ', 10),
(138, 'cumin', 10),
(139, 'gingembre ', 10),
(140, 'cannelle', 10),
(141, 'pimant de Cayenne', 10),
(142, 'vanille', 10),
(143, 'citronnelle', 10),
(144, 'piment frais', 10),
(145, 'salade', 2);

-- --------------------------------------------------------

--
-- Structure de la table `plat`
--

CREATE TABLE `plat` (
  `id` int(11) NOT NULL,
  `nomPlat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `plat`
--

INSERT INTO `plat` (`id`, `nomPlat`, `prix`) VALUES
(1, 'Gratin d\'aubergines aux tomates et au chèvre', 15),
(2, 'Mille feuille d\'aubergine au chèvre et légumes grillès', 12),
(3, 'Tartines de legumes grillès ', 10),
(4, 'Salade printanière d\'endives et d\'asperges', 11),
(5, 'Pavé de Saumon écossais poêlée de pommes ratte et asperges vertes au safran', 18),
(6, 'Bavarois d\'asperge au saumon et ricotta', 12);

-- --------------------------------------------------------

--
-- Structure de la table `plat_ingredient`
--

CREATE TABLE `plat_ingredient` (
  `id` int(11) NOT NULL,
  `remarque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plat_id` int(11) DEFAULT NULL,
  `ingredient_id` int(11) DEFAULT NULL,
  `nbrIngredient` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `plat_ingredient`
--

INSERT INTO `plat_ingredient` (`id`, `remarque`, `plat_id`, `ingredient_id`, `nbrIngredient`) VALUES
(1, NULL, 1, 1, NULL),
(2, NULL, 1, 21, NULL),
(3, NULL, 1, 53, NULL),
(4, NULL, 2, 1, NULL),
(5, NULL, 2, 53, NULL),
(6, NULL, 2, 21, NULL),
(7, NULL, 3, 1, NULL),
(8, NULL, 3, 53, NULL),
(9, NULL, 3, 21, NULL),
(10, NULL, 6, 2, NULL),
(11, NULL, 6, 66, NULL),
(12, NULL, 6, 45, NULL),
(13, NULL, 5, 66, NULL),
(14, NULL, 5, 17, NULL),
(15, NULL, 5, 2, NULL),
(16, NULL, 4, 9, NULL),
(17, NULL, 4, 2, NULL),
(18, NULL, 4, 145, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

CREATE TABLE `statut` (
  `id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C7440455F6203804` (`statut_id`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6EEAA67D19EB6921` (`client_id`);

--
-- Index pour la table `delai_plat`
--
ALTER TABLE `delai_plat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_133381A182EA2E54` (`commande_id`),
  ADD KEY `IDX_133381A1D73DB560` (`plat_id`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C53D045FD73DB560` (`plat_id`);

--
-- Index pour la table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6BAF7870BCF5E72D` (`categorie_id`);

--
-- Index pour la table `plat`
--
ALTER TABLE `plat`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `plat_ingredient`
--
ALTER TABLE `plat_ingredient`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E0ED47FBD73DB560` (`plat_id`),
  ADD KEY `IDX_E0ED47FB933FE08C` (`ingredient_id`);

--
-- Index pour la table `statut`
--
ALTER TABLE `statut`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `delai_plat`
--
ALTER TABLE `delai_plat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;
--
-- AUTO_INCREMENT pour la table `plat`
--
ALTER TABLE `plat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `plat_ingredient`
--
ALTER TABLE `plat_ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `statut`
--
ALTER TABLE `statut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `FK_C7440455F6203804` FOREIGN KEY (`statut_id`) REFERENCES `statut` (`id`);

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `FK_6EEAA67D19EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Contraintes pour la table `delai_plat`
--
ALTER TABLE `delai_plat`
  ADD CONSTRAINT `FK_133381A182EA2E54` FOREIGN KEY (`commande_id`) REFERENCES `commande` (`id`),
  ADD CONSTRAINT `FK_133381A1D73DB560` FOREIGN KEY (`plat_id`) REFERENCES `plat` (`id`);

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `FK_C53D045FD73DB560` FOREIGN KEY (`plat_id`) REFERENCES `plat` (`id`);

--
-- Contraintes pour la table `ingredient`
--
ALTER TABLE `ingredient`
  ADD CONSTRAINT `FK_6BAF7870BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `plat_ingredient`
--
ALTER TABLE `plat_ingredient`
  ADD CONSTRAINT `FK_E0ED47FB933FE08C` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`),
  ADD CONSTRAINT `FK_E0ED47FBD73DB560` FOREIGN KEY (`plat_id`) REFERENCES `plat` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
