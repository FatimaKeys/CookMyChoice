document.addEventListener('DOMContentLoaded', function () { 
   var selectCat1 = document.getElementById('categorie1');
   var selectIngredient1 = document.getElementById('ingredient1');
   
   var selectCat2 = document.getElementById('categorie2');
   var selectIngredient2 = document.getElementById('ingredient2');
   
   var selectCat3 = document.getElementById('categorie3');
   var selectIngredient3 = document.getElementById('ingredient3');
    //ajax pour trouver
    selectCat1.addEventListener('change', function(e){trouverIngredient(selectIngredient1,selectCat1);
    });
    selectCat2.addEventListener('change', function(e){trouverIngredient(selectIngredient2,selectCat2);
    });
    selectCat3.addEventListener('change', function(e){trouverIngredient(selectIngredient3,selectCat3);
    });
    
    function trouverIngredient(selectIng,selectCat){
        while (selectIng.hasChildNodes()) {  
                selectIng.removeChild(selectIng.firstChild);
            }

        var userChoice = selectCat.options[selectCat.selectedIndex].value;
        console.log(userChoice);
        $.ajax({
            url: "/trouverIngredientAjax",
            method: "POST", 
            data: {
               "userChoice": userChoice                
            },
            dataType: 'json'
        })
        .done(function (data) {
            console.log(data);
                var option = document.createElement('option');
                option.setAttribute('value',"" );
                option.text="choisir un "+userChoice;
                selectIng.appendChild(option);
            for (var i = 0; i < data.nomIngredients.length; i++) {
                var option = document.createElement('option');
                option.setAttribute('value',data.nomIngredients[i] );
                option.text=data.nomIngredients[i];
                selectIng.appendChild(option);
            }

           
        });
    }
   
});


